#include <iostream>
#include <fstream>
#include <string>


using namespace std;

struct Map
{
	char symbol;
	int number;
	static const int count = 26;
};

Map* createMap();
void writeMapToFile(string, Map*);
void displayContentFileWithMap(string);
void encoding(string, string, Map*);
void decoding(string, string, Map*);
Map* createDectionary(string);
int encoding(char, Map*);
char decoding(int, Map*);

int main()
{
	Map* map = createMap();
	writeMapToFile("codes", map);
	displayContentFileWithMap("codes");
	map = createDectionary("codes");
	encoding("input.txt", "result.txt", map);
	map = createDectionary("codes");
	decoding("result.txt", "output.txt", map);
}

Map* createMap()
{
	Map* mapList = new Map[Map::count];
	char symbol = 'A';
	int number = 26;
	for (size_t i = 0; i < Map::count; i++, symbol++, number--)
	{
		mapList[i].symbol = symbol;
		mapList[i].number = number;
	}
	return mapList;
}

void writeMapToFile(string fileName, Map* map)
{
	ofstream fout(fileName, ios::out | ios::binary);
	if (!fout.is_open())
	{
		throw std::exception();
	}
	int size = sizeof(Map) * Map::count;
	fout.write((char*)map, size);
	fout.close();
}

void displayContentFileWithMap(string fileName)
{
	ifstream fin(fileName, ios::binary);
	if (!fin.is_open())
	{
		throw std::exception();
	}
	Map* map = new Map[Map::count];
	int size = sizeof(Map) * Map::count;
	fin.read((char*)map, size);
	for (size_t i = 0; i < Map::count; i++)
	{
		cout << map[i].symbol << " - " << map[i].number << endl;
	}
	fin.close();
}

Map* createDectionary(string fileName)
{
	ifstream fin(fileName, ios::binary);
	if (!fin.is_open())
	{
		throw std::exception();
	}
	Map* map = new Map[Map::count];
	int size = sizeof(Map) * Map::count;
	fin.read((char*)map, size);
	fin.close();
	return map;
}

int encoding(char symbol, Map* map)
{
	for (size_t i = 0; i < Map::count; i++)
	{
		if (map[i].symbol == symbol)
		{
			return map[i].number;
		}
	}
	return (int)symbol;
}

void encoding(string input, string output, Map* map)
{
	ofstream fout(output);
	ifstream fin(input);
	if (!fout.is_open() || !fin.is_open())
	{
		throw std::exception();
	}
	int size = sizeof(char);
	char tmp;
	while (!fin.eof())
	{
		fin.read((char*)&tmp, size);
		tmp = toupper(tmp);
		if (fin.eof()) return;
		int num = encoding(tmp, map);
		fout << num << ' ';
	}
	fin.close();
	fout.close();
}

void decoding(string input, string output, Map* map)
{
	ofstream fout(output);
	ifstream fin(input);
	if (!fout.is_open() || !fin.is_open())
	{
		throw std::exception();
	}
	string line;
	while (getline(fin, line))
	{
		int start = 0;
		for (size_t i = 0; i < line.length(); i++)
		{
			if (line[i] == ' ')
			{
				int id = stoi(line.substr(start, i-start));
				start = i + 1;
				char symbol = decoding(id, map);
				fout << symbol;
			}
		}
	}
	fin.close();
	fout.close();
}

char decoding(int item, Map* map)
{
	for (size_t i = 0; i < Map::count; i++)
	{
		if (map[i].number == item)
		{
			return map[i].symbol;
		}
	}
	return (char)item;
}
