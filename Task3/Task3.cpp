#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int binaryToDecimal(string);
int calculateSum(string);
int calculateSumFromFile(string);
void calculateSumTest(string);
bool isBinaryNumber(char);

int main()
{
	calculateSumTest("test.txt");
}

int calculateSum(string source)
{
	int sum = 0, start = 0;
	for (size_t i = 0; i < source.length(); i++)
	{
		if (!isBinaryNumber(source[i]) && isBinaryNumber(source[i+1]))
		{
			start = i + 1;
		}
		if (isBinaryNumber(source[i]) && !isBinaryNumber(source[i+1]))
		{

			sum += binaryToDecimal(source.substr(start, i - start + 1));
		}
	}
	return sum;
}

bool isBinaryNumber(char symbol) {
	return symbol == '0' || symbol == '1';
}

int calculateSumFromFile(string fileName)
{
	ifstream fin(fileName);
	if (!fin.is_open())
	{
		throw std::exception();
	}
	string line;
	int sum = 0;
	while (getline(fin, line))
	{
		sum += calculateSum(line);
	}
	fin.close();
	return sum;
}

int binaryToDecimal(string binary)
{
	int decimal = 0;
	for (size_t i = 0; i < binary.length(); i++) {
		if (binary[i] == '1') {
			decimal += pow(2, binary.length() - i - 1);
		}
	}
	return decimal;
}

void calculateSumTest(string fileName)
{
	bool result = calculateSum("1+-0100+*** 1000") == 13;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1000001+-1000+* 100** 1--- 0000001") == 79;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("10000000000011111+11+*** 1111000111") == 66537;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1111111111111000011111+-11111111111+*   1111** -22--- ") == 4195885;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSumFromFile(fileName) == 4262515;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
}
